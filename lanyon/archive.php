<?php get_template_part( 'lanyon/header' ); ?>
<?php get_template_part( 'lanyon/sidebar' ); ?>

<div class="wrap">
  <?php get_template_part( 'lanyon/title' ); ?>
  <?php get_template_part( 'lanyon/content', 'archive' ); ?>
</div>

<?php get_template_part( 'lanyon/footer' ); ?>
