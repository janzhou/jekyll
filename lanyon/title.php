  <div class="masthead">
    <div class="container">
    <h3 class="masthead-title">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
        <?php $description = get_bloginfo( 'description', 'display' );
        if ( $description || is_customize_preview() ) : ?>
        <small><?php echo $description; /* WPCS: xss ok. */ ?></small>
        <?php endif; ?>
    </h3>
    </div>
  </div>


