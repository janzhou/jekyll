<input type="checkbox" class="sidebar-checkbox" id="sidebar-checkbox">

<div class="sidebar" id="sidebar">

  <div class="sidebar-item">
    <p>
      This <a href="https://janzhou.org/theme/jekyll">Theme</a> is ported from Jekyll theme <a href="https://github.com/poole/lanyon">lanyon</a> to Wordpress.
    </p>
  </div>

  <div class="sidebar-item">
    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
      <input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </form>
  </div>

  <nav class="sidebar-nav">
    <?php
      $menuParameters = array(
        'theme_location'  => 'lanyon',
        'menu_id'         => 'lanyon-menu',
        'container'       => false,
        'echo'            => false,
        'items_wrap'      => '%3$s',
        'depth'           => 0,
      );

      echo str_replace('href=', 'class="sidebar-nav-item" href=', strip_tags(wp_nav_menu( $menuParameters ), '<a>' ));
    ?>
  </nav>

  <div class="sidebar-item">
    <p>
      &copy; 2014. All rights reserved.
    </p>
  </div>
</div>
