<div class="pagination">
    <?php
      function next_posts_link_attributes(){
        return 'class="pagination-item older"';
      }
      add_filter('next_posts_link_attributes', 'next_posts_link_attributes');
      $next = get_next_posts_link('Older');
      if( $next == null ) {
        $next = '<span class="pagination-item older">Older</span>';
      }
      echo $next;

      function previous_posts_link_attributes(){
        return 'class="pagination-item newer"';
      }
      add_filter('previous_posts_link_attributes', 'previous_posts_link_attributes');
      $previous = get_previous_posts_link('Newer');
      if( $previous == null ) {
        $previous = '<span class="pagination-item newer">Newer</span>';
      }
      echo $previous;
     ?>
</div>
