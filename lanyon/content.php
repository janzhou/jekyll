<?php if (have_posts()) : while (have_posts()) : the_post();?>

  <div id="post-<?php the_ID(); ?>" class="container content">
    <div class="posts">
      <h1 class="post-title">
        <a href="<?php esc_url( get_permalink() ); ?>">
          <?php the_title(); ?>
        </a>
      </h1>
    
      <span class="post-date">
      <?php jekyll_date(); ?>
      </span>
    
      <?php the_content(); ?>
    
      <?php get_template_part( 'lanyon/comments' ); ?>
    </div>
  </div>

<?php endwhile; endif; ?>
