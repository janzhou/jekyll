<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package minimal
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/lanyon/poole.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/lanyon/lanyon.css" type="text/css" media="screen" />

<?php wp_head(); ?>
</head>

<body>
