<?php if (have_posts()) : while (have_posts()) : the_post();?>

  <div id="post-<?php the_ID(); ?>" class="container content">
    <div class="posts">
      <h1 class="post-title">
        <?php the_title(); ?>
      </h1>

      <?php the_content(); ?>
      <?php echo jekyll_list_child_pages(); ?>

      <?php get_template_part( 'lanyon/comments' ); ?>
    </div>
  </div>

<?php endwhile; endif; ?>
