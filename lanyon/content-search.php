<div class="container content">
  <div class="posts">
    <h1 class="post-title">
      <?php printf( esc_html__( 'Search Results for: %s', 'jekyll' ), '<span>' . get_search_query() . '</span>' ); ?>
    </h1>

    <?php if (have_posts()) : ?>
    <ul>
      <?php while (have_posts()) : the_post();?>
        <li>
          <a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a>
        </li>
      <?php endwhile; ?>
    </ul>

    <?php else : ?>
      Nothing Find.
    <?php endif; ?>

    <?php get_template_part( 'lanyon/pagination' ); ?>

  </div>
</div>
