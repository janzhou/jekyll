<div class="container content">
  <div class="posts">
    <h1 class="post-title">
      <?php the_archive_title(); ?>
    </h1>
					<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
    <ul>
      <?php if (have_posts()) : while (have_posts()) : the_post();?>
        <li>
          <p>
            <?php echo esc_html( get_the_date('d M Y') ); ?>
            &raquo;
            <a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a>
          </p>
        </li>
      <?php endwhile; endif; ?>
    </ul>

    <?php get_template_part( 'lanyon/pagination' ); ?>

  </div>
</div>
